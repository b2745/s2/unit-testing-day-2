const chai = require('chai');
const expect =chai.expect
const http = require('chai-http');

chai.use(http);

describe('API Test Suite',() => {
	it('Test API Get people is running', () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test API get people return 200', () => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200)
		})
	})


	it('Test API post person return 400 if no NAME property',(done)=> {
		chai.request('http://localhost5001')
		.post('/person')
		.type('json')
		.send({
			name: "Mikasa",
			age: 27 
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})
			done()
	})


	it('Test API POST endpoint is running', () => {
		chai.request('http://localhost:5001')
		.post('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200)
		})
	})


	it('Test that an endpoint encounters an error if no alias', (done) => {
		chai.request('http://localhost5001')
		.post('/person')
		.type('json')
		.send({
			name: "Mikasa",
			age: 27 
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})
		done()
	})


	it('Test that an endpoint encounters an error if no age', (done) => {
		chai.request('http://localhost5001')
		.post('/person')
		.type('json')
		.send({
			name: "Mikasa",
			gender: "female"

		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})
		done();
	})
})

