const { factorial, div_check } = require ('../src/utils.js');
// const { div_check } = require ('../src/utils.js');
const { expect, assert } = require('chai');



 describe('Test factorials', () => {

it('Test that 5! is 120', () => {
	const product = factorial(5)
	expect(product).to.equal(120)
});

it('Test that 1! is 1', () => {
	const product = factorial(1)
	expect(product, 1)
 });

it('Test that 0! is 1', () => {
	const product = factorial(0)
	expect(product).to.equal(1)
});
it('Test that 4! is 24', () => {
	const product = factorial(4)
	expect(product).to.equal(24)
});

it('Test that 10! is 3628800', () => {
	const product = factorial(10)
	expect(product).to.equal(3628800)
});

it('test negative factorial is undefined', () => {
	const product = factorial(-1);
	expect(product).to.equal(undefined);
});
it('test negative factorial is none numeric', () => {
	const product = factorial("asd");
	expect(product).to.equal(number);
})

});

 describe('Check Divisibility', () => {

it('Test that 105 is divisible by 5',() => {
	const quotient = div_check(105)
	expect(quotient).to.equal(5)
});
it('Test that 14 is divisible by 7',() => {
	const quotient = div_check(14)
	expect(quotient).to.equal(7)
});
it('Test that 0 is divisible by both 5 and 7',() => {
	const quotient = div_check(0)
	expect(quotient).to.equal(5,7)
});
it('Test that 22 is NOT divisible by 5 or 7',() => {
	const quotient = div_check(22)
	expect(quotient).is.not.equal(5,7)
});


});

