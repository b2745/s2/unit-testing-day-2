const factorial = (n) => {
	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n-1);
};
		
const div_check = (n) => {
	if(n % 5 === 0 ) return 5;
	if(n % 7 === 0) return 7; 
	return n / div_check(n-1);

}

const names = {
	"Eren" : {
		"name" : "Eren Yeager",
		"age" : 28
	},
	"Levi" : {
		"name" : "Levi Akcerman",
		"age" : 38
	}
};

module.exports = { 
	factorial: factorial,
	div_check: div_check,
	names : names

}
